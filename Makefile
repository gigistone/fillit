NAME = fillit

LIBFT_DIR := libft
LIBFT := $(LIBFT_DIR)/libft.a
LIBFT_HEADERS := $(LIBFT_DIR)/includes
INCDIR := inc
TARGETDIR := .
BUILD_DIR := build
OBJDIR := $(BUILD_DIR)/obj
SRCDIR := src

CC := gcc
LINK := gcc
CFLAGS += -Wall -Wextra
CFLAGS += -I $(INCDIR)/
CFLAGS += -I $(LIBFT_HEADERS)/

SRC += $(SRCDIR)/utils.c
SRC += $(SRCDIR)/map.c
SRC += $(SRCDIR)/main.c
SRC += $(SRCDIR)/tetri_parser.c
SRC += $(SRCDIR)/tetrimino.c
SRC += $(SRCDIR)/solver.c

OBJ = $(SRC:$(SRCDIR)%.c=$(OBJDIR)%.o)

RM := rm -f
MKDIR := mkdir -p
MAKE = make -C

all : $(NAME)

$(NAME) : $(OBJDIR) $(LIBFT) $(OBJ)
	@$(LINK) $(OBJ) -o $@ -L $(LIBFT_DIR) -lft
	@echo "$(NAME) built."

$(OBJDIR)/%.o : $(SRCDIR)/%.c
	@$(CC) $(CFLAGS) -c $^ -o $@

clean :
	@$(RM) $(OBJ)
	@$(MAKE) $(LIBFT_DIR) clean
	@echo "Objects files cleaned"

fclean : clean
	@$(RM) $(NAME)
	@$(MAKE) $(LIBFT_DIR) fclean
	@echo "Target cleaned"

re : fclean all

$(OBJDIR) :
	@echo "Build directory doesnt exists..."
	@echo "Try to create directories..."
	@$(MKDIR) $@
	@echo "$(OBJDIR) created"

$(LIBFT) :
	@echo "Build libft ..."
	@$(MAKE) $(LIBFT_DIR) re 1>/dev/null
	@echo "Libft Built"