/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 14:52:10 by bboutoil          #+#    #+#             */
/*   Updated: 2018/12/10 10:00:23 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "map.h"
#include "utils.h"

/*
** DESTROY_INTERNAL_ARRAY
** Destroy internal storage of map structure.
** Ret: N/A
*/

static void	destroy_internal_array(t_map *map)
{
	int			row;
	int			size;

	row = 0;
	size = map->size;
	while (row < size)
		free(map->map[row++]);
	free(map->map);
}

/*
** CREATE_INTERNAL_ARRAY
** Create and initialise internal storage of map structure.
** Nb : If memory error while allocating, free all rows
** and destroy structure.
** Ret: adress of array, NULL on failure
*/

static char	**create_internal_array(int size)
{
	int		row;
	char	**arr;

	if ((arr = (char **)malloc(sizeof(char *) * size)) == NULL)
		return (NULL);
	row = 0;
	while (row < size)
	{
		if ((arr[row] = (char *)malloc(size)) == NULL)
			break ;
		ft_memset(arr[row++], '.', size);
	}
	if (row + 1 < size)
	{
		while (row-- > 0)
			free(arr[row]);
		free(arr);
		return (NULL);
	}
	return (arr);
}

/*
** CREATE_MAP
** Create a map. A map is a two dimensial array stocking tetriminos.
** Ret: Map object. NULL on failure
*/

t_map		*create_map(int size)
{
	t_map	*map;

	if ((map = (t_map *)malloc(sizeof(t_map))) == NULL)
		return (NULL);
	map->size = size;
	if ((map->map = create_internal_array(size)) == NULL)
		return (NULL);
	return (map);
}

/*
** DESTROY_MAP
** Destroy the map.
** Ret: N/A
*/

void		destroy_map(t_map *map)
{
	destroy_internal_array(map);
	free(map);
}

/*
** ENLARGE_MAP
** Encrease the size of map by 1.
** Ret: N/A
*/

int			enlarge_map(t_map *map)
{
	destroy_internal_array(map);
	map->size += 1;
	if ((map->map = create_internal_array(map->size)) == NULL)
		return (ERROR);
	return (SUCCESS);
}
