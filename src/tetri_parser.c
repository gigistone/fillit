/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetri_parser.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chrhuang <chrhuang@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/29 15:41:25 by bboutoil          #+#    #+#             */
/*   Updated: 2018/12/10 11:35:42 by chrhuang         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tetri_parser.h"
#include "libft.h"
#include "get_next_line.h"
#include "tetrimino.h"
#include "utils.h"
#include <stdlib.h>

int					check_tetrimino(char (*shape)[5], int i)
{
	int	nb;
	int	x;
	int	y;

	nb = 0;
	y = -1;
	while (++y < 4)
	{
		x = -1;
		while (++x < 4)
		{
			if (shape[y][x] != '#' && shape[y][x] != '.')
				return (FAIL);
			else if (shape[y][x] == '#')
			{
				i = i + 1;
				nb = x > 0 && shape[y][x - 1] == '#' ? nb + 1 : nb;
				nb = x < 3 && shape[y][x + 1] == '#' ? nb + 1 : nb;
				nb = y > 0 && shape[y - 1][x] == '#' ? nb + 1 : nb;
				nb = y < 3 && shape[y + 1][x] == '#' ? nb + 1 : nb;
			}
		}
	}
	return (nb >= 6 && i == 4 ? SUCCESS : FAIL);
}

static t_tetrimino	*create_list_bis(t_tetrimino *curr, int *i,
								char *str, t_tetrimino *begin)
{
	ft_strcpy(curr->shape[*i], str);
	if (*i != 0 && *i % 4 == 0)
	{
		if ((check_tetrimino(curr->shape, 0) != SUCCESS) || (str[0] != 0))
		{
			free(str);
			destroy_tetriminos((t_tetrimino *)begin);
			return (NULL);
		}
		curr->next = new_tetrimino();
		curr = curr->next;
		*i = -1;
	}
	return (curr);
}

static t_tetrimino	*create_list(int fd, char *str)
{
	const	t_tetrimino	*begin = new_tetrimino();
	t_tetrimino			*curr;
	int					i;

	i = -1;
	if ((curr = (t_tetrimino *)begin) == NULL)
		return (NULL);
	while (++i < 5 && get_next_line(fd, &str))
	{
		if (!(ft_strlen(str) == 0 || ft_strlen(str) == 4) ||
			(ft_strlen(str) != 4 && i % 4 != 0))
		{
			destroy_tetriminos((t_tetrimino *)begin);
			free(str);
			return (NULL);
		}
		if ((curr = create_list_bis(curr, &i, str,
									(t_tetrimino *)begin)) == NULL)
			return (NULL);
		free(str);
	}
	return (check_tetrimino(curr->shape, 0) != SUCCESS || i % 4 != 0 ?
			NULL : (t_tetrimino *)begin);
}

t_tetrimino			*parse_file(char *file)
{
	int			fd;
	t_tetrimino	*tetriminos;
	char		*str;

	str = NULL;
	if ((fd = open(file, O_RDONLY)) == -1)
		return (NULL);
	if ((tetriminos = create_list(fd, str)) == NULL)
		return (NULL);
	epur_tetrimino_width(tetriminos);
	epur_tetrimino_height(tetriminos);
	add_point(tetriminos);
	tetriminos->height = 0;
	tetriminos->width = 0;
	if (add_width_height_name(tetriminos, 'A') == FAIL)
	{
		destroy_tetriminos(tetriminos);
		return (NULL);
	}
	close(fd);
	return (tetriminos);
}
