/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chrhuang <chrhuang@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 13:25:58 by bboutoil          #+#    #+#             */
/*   Updated: 2018/12/10 11:25:01 by chrhuang         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"
#include "tetrimino.h"
#include "utils.h"
#include "map.h"

int			add_width_height_name(t_tetrimino *tetriminos, char name)
{
	int		y;
	int		x;

	while (tetriminos != NULL)
	{
		y = -1;
		while (++y < 4)
		{
			x = -1;
			while (++x < 4)
				if (tetriminos->shape[y][x] == '#')
				{
					if (tetriminos->height < y)
						tetriminos->height = y;
					if (tetriminos->width < x)
						tetriminos->width = x;
				}
		}
		tetriminos->name = name++;
		tetriminos = tetriminos->next;
	}
	return (name > 'Z' + 1 ? FAIL : SUCCESS);
}

void		add_point(t_tetrimino *tetriminos)
{
	int		x;
	int		y;
	int		i;

	while (tetriminos != NULL)
	{
		i = 0;
		y = 0;
		while (y < 4)
		{
			x = 0;
			while (x < 4)
			{
				if (tetriminos->shape[y][x] == '#')
				{
					tetriminos->point[i].x = x;
					tetriminos->point[i].y = y;
					i = i + 1;
				}
				x = x + 1;
			}
			y = y + 1;
		}
		tetriminos = tetriminos->next;
	}
}

/*
** PRINT_MAP
** Print map on stdoutput.
** Ret: N/A
*/

void		print_map(t_map *map)
{
	int	i;

	i = 0;
	while (i < map->size)
	{
		write(1, map->map[i], map->size);
		write(1, "\n", 1);
		++i;
	}
}
