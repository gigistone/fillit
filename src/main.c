/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 19:43:08 by bboutoil          #+#    #+#             */
/*   Updated: 2018/12/10 11:01:23 by chrhuang         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "app_config.h"
#include "utils.h"
#include "tetri_parser.h"
#include "solver.h"
#include "map.h"

int		main(int argc, char const *argv[])
{
	t_tetrimino	*tetriminos;

	if (argc != ALLOWED_ARGS)
	{
		if (argc < ALLOWED_ARGS)
			ft_putendl(MSG_ERR_MISSING_FILE);
		else
			ft_putendl(MSG_ERR_TOO_MANY_ARGS);
		ft_putendl(MSG_CMD_USAGE);
	}
	if ((tetriminos = parse_file((char *)argv[1])) == NULL)
	{
		ft_putendl(MSG_ERROR);
		return (-1);
	}
	solver(tetriminos);
	destroy_tetriminos(tetriminos);
	return (0);
}
