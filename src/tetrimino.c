/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetrimino.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 10:23:49 by bboutoil          #+#    #+#             */
/*   Updated: 2018/12/10 11:25:59 by chrhuang         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "tetrimino.h"
#include "utils.h"
#include "libft.h"

void		epur_tetrimino_height(t_tetrimino *tetriminos)
{
	int	i;
	int	j;

	while (tetriminos != NULL)
	{
		while ((tetriminos->shape[0][0] == '.' &&
				tetriminos->shape[0][1] == '.' &&
				tetriminos->shape[0][2] == '.' &&
				tetriminos->shape[0][3] == '.'))
		{
			j = -1;
			while (++j < 3)
			{
				i = -1;
				while (++i < 4)
					tetriminos->shape[j][i] = tetriminos->shape[j + 1][i];
			}
			ft_strcpy(tetriminos->shape[3], "....");
		}
		tetriminos = tetriminos->next;
	}
}

void		epur_tetrimino_width(t_tetrimino *tetriminos)
{
	int				i;
	int				j;

	while (tetriminos != NULL)
	{
		while ((tetriminos->shape[0][0] == '.' &&
				tetriminos->shape[1][0] == '.' &&
				tetriminos->shape[2][0] == '.' &&
				tetriminos->shape[3][0] == '.'))
		{
			j = -1;
			while (++j < 4)
			{
				i = -1;
				while (++i < 3)
					tetriminos->shape[j][i] = tetriminos->shape[j][i + 1];
				tetriminos->shape[j][3] = '.';
			}
		}
		tetriminos = tetriminos->next;
	}
}

t_tetrimino	*new_tetrimino(void)
{
	t_tetrimino	*tetrimino;
	int			i;

	i = 0;
	if ((tetrimino = malloc(sizeof(t_tetrimino))) == NULL)
		return (NULL);
	while (i < 4)
	{
		tetrimino->shape[i][4] = '\0';
		i = i + 1;
	}
	tetrimino->next = NULL;
	tetrimino->width = ERROR;
	tetrimino->height = ERROR;
	return (tetrimino);
}

void		destroy_tetriminos(t_tetrimino *tetriminos)
{
	t_tetrimino	*tmp;

	while (tetriminos != NULL)
	{
		tmp = tetriminos;
		tetriminos = tetriminos->next;
		free(tmp);
	}
}
