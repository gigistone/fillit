/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solver.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 19:34:36 by bboutoil          #+#    #+#             */
/*   Updated: 2018/12/10 11:34:33 by chrhuang         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "tetri_parser.h"
#include "map.h"
#include "tetrimino.h"

static int	ft_nb_tetri(t_tetrimino *tetriminos)
{
	int		size;
	int		nb;

	size = 0;
	nb = 2;
	while (tetriminos != NULL)
	{
		tetriminos = tetriminos->next;
		size = size + 1;
	}
	while (nb * nb < size * 4)
		nb = nb + 1;
	return (nb);
}

static int	put_tetri(t_tetrimino *tetriminos, t_map *map, t_pos pos, char c)
{
	int		i;

	i = 0;
	while (i < 4)
	{
		map->map[pos.y + tetriminos->point[i].y]
		[pos.x + tetriminos->point[i].x] = c;
		i = i + 1;
	}
	return (SUCCESS);
}

static int	is_putable(t_tetrimino *tetriminos, t_map *map, t_pos pos)
{
	int		i;

	i = 0;
	while (i < 4)
	{
		if (map->map[pos.y + tetriminos->point[i].y]
			[pos.x + tetriminos->point[i].x] != '.')
			return (FAIL);
		i = i + 1;
	}
	put_tetri(tetriminos, map, pos, tetriminos->name);
	return (SUCCESS);
}

static int	do_loop(t_tetrimino *tetriminos, t_map *map)
{
	t_pos	pos;

	if (tetriminos == NULL)
		return (SUCCESS);
	pos.y = 0;
	while (pos.y < map->size - tetriminos->height)
	{
		pos.x = 0;
		while (pos.x < map->size - tetriminos->width)
		{
			if (is_putable(tetriminos, map, pos) == SUCCESS)
			{
				if (do_loop(tetriminos->next, map) == 1)
					return (SUCCESS);
				else
					put_tetri(tetriminos, map, pos, '.');
			}
			pos.x = pos.x + 1;
		}
		pos.y = pos.y + 1;
	}
	return (FAIL);
}

int			solver(t_tetrimino *tetriminos)
{
	int		size;
	t_map	*map;

	size = ft_nb_tetri(tetriminos);
	map = create_map(size);
	while (do_loop(tetriminos, map) != SUCCESS)
		if (enlarge_map(map) == ERROR)
			return (ERROR);
	print_map(map);
	destroy_map(map);
	return (SUCCESS);
}
