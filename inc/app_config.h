/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app_config.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 11:07:41 by bboutoil          #+#    #+#             */
/*   Updated: 2018/12/10 11:01:17 by chrhuang         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef APP_CONFIG_H
# define APP_CONFIG_H

# define BUILD_DEBUG
# define PROGRAM_NAME "fillit"
# define ALLOWED_ARGS (2)
# define MSG_ERR_TOO_MANY_ARGS ""PROGRAM_NAME": Too many args."
# define MSG_ERR_MISSING_FILE ""PROGRAM_NAME": Missing file operand."
# define MSG_CMD_USAGE "Try './"PROGRAM_NAME" [filename]'"
# define MSG_ERROR "error"

#endif
