/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetrimino.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/29 16:03:55 by bboutoil          #+#    #+#             */
/*   Updated: 2018/12/10 10:43:30 by chrhuang         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TETRIMINO_H
# define TETRIMINO_H

# include "libft.h"

typedef struct			s_tetrimino
{
	char				name;
	char				shape[4][5];
	t_pos				point[4];
	int					width;
	int					height;
	struct s_tetrimino	*next;
}						t_tetrimino;

t_tetrimino				*tetrimino_create(char *shape, int width, int height);
void					tetrimino_destroy(t_tetrimino **mino);
void					tetrimino_show_shape(t_tetrimino *tetri);
t_tetrimino				*new_tetrimino(void);
void					epur_tetrimino_height(t_tetrimino *tetriminos);
void					epur_tetrimino_width(t_tetrimino *tetriminos);
void					destroy_tetriminos(t_tetrimino *tetriminos);

#endif
