/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 14:53:00 by bboutoil          #+#    #+#             */
/*   Updated: 2018/12/09 15:40:42 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_H
# define MAP_H

typedef struct	s_map
{
	int			size;
	char		**map;
}				t_map;

t_map			*create_map(int size);
void			destroy_map(t_map *map);
int				enlarge_map(t_map *map);

#endif
