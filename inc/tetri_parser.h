/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetri_parser.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/29 15:41:29 by bboutoil          #+#    #+#             */
/*   Updated: 2018/12/09 14:42:46 by chrhuang         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TETRI_PARSER_H
# define TETRI_PARSER_H

# include <fcntl.h>
# include "tetrimino.h"

# define MAX_TETRIMINOS_PER_FILE (26)
# define MAX_CHAR_PER_LINE (4)
# define MAX_LINE_PER_TETRIMINO (4)
# define TETRI_COMPONENT_CHAR '#'
# define TETRI_GRID_CHAR '.'
# define SEPARATOR_BETWEEN_TETRIMINOS '\n'

t_tetrimino				*parse_file(char *file);
int						check_tetrimino(char (*shape)[5], int i);

#endif
