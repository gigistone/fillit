/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 19:42:56 by bboutoil          #+#    #+#             */
/*   Updated: 2018/12/10 11:08:23 by chrhuang         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

# include "map.h"
# include "tetrimino.h"
# include <unistd.h>

# define SUCCESS	1
# define FAIL		0
# define ERROR		-1

void	puts_error(const char *err);
void	print_map(t_map *map);
int		add_width_height_name(t_tetrimino *tetriminos, char name);
void	add_point(t_tetrimino *tetriminos);

#endif
